﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml.Serialization;

namespace Comfy.RazorClassLibrary.Models
{
    [Serializable]
    public class Customer
    {
        public string Name { get; set; }
        public string Phone { get; set; }
        public List<BonusCard> BonusCards { get; set; }

        protected void Save()
        {
            XmlSerializer writer = new XmlSerializer(typeof(BonusCard));
            var path = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + $"//Customers.xml";
            System.IO.FileStream file = System.IO.File.Create(path);

            writer.Serialize(file, this);
            file.Close();
        }        
    }    
}
