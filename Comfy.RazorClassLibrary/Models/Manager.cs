﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Serialization;

namespace Comfy.RazorClassLibrary.Models
{
    public class Manager
    {
        public int MaxLimitCard { get; set; }
        private static Manager _instance;
        public List<BonusCard> BonusCards { get; set; } = new List<BonusCard>();
        private string File { get; set; }
        private Manager(string basefile, int maxLimitCard)
        {
            MaxLimitCard = maxLimitCard;
            File = basefile;
        }

        public static Manager GetInstance(string filebase, int maxLimit)
        {
            if (_instance == null)
            {
                _instance = new Manager(filebase, maxLimit);
                _instance.LoadBonusCards();
            }

            //init adding first and last cards to collection
            if (_instance.BonusCards.Count()==0)
            {
                _instance.BonusCards.Add(new BonusCard() { Id = 0, Balance = 0, Customer = new Customer() {  Name = "Name", Phone = "0000000000"}, ExpiryDate = DateTime.Now });
                _instance.BonusCards.Add(new BonusCard() { Id = maxLimit, Balance = 0, Customer = new Customer() { Name = "Name", Phone = "0000000000" }, ExpiryDate = DateTime.Now });
            }
            return _instance;
        }

        private void LoadBonusCards()
        {
            var serializer = new XmlSerializer(typeof(List<BonusCard>));
            var path = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + $"//{File}.xml";
            if (System.IO.File.Exists(path))
            {
                try
                {
                    using (StreamReader sr = new StreamReader(path))
                    {
                        BonusCards.AddRange((List<BonusCard>)serializer.Deserialize(sr));
                    }
                }
                catch (Exception)
                {
                }
            }
            else
            {
                Save();
            }
        }

        public void DeleteBonusCard(int id) 
        {
            var card = this.FindBonusCard(id);
            if (card!=null)
            {
                BonusCards.Remove(card);
            }        
        }

        public int AddNewBonusCard(string name, string phone, double startBalance) 
        {
            Customer customer = new Customer() { Name = name, Phone = phone };
            BonusCard card = new BonusCard(customer, DateTime.Now.AddDays(365), startBalance);

            try
            {
                card.Id = GetNewId(0, MaxLimitCard);
            }
            catch (Exception)
            {
                throw;
            }

            BonusCards.Add(card);
            BonusCards = BonusCards.OrderBy(x => x.Id).ToList();
            Save();
            return card.Id;
        }

        public void AddNewBonusCard(string name, string phone, double startBalance, int id) 
        {
            Customer customer = new Customer() { Name = name, Phone = phone };
            BonusCard card = new BonusCard(customer, DateTime.Now.AddDays(365), startBalance)
            {
                Id = id
            };
            BonusCards.Add(card);
        }

        //private int getNewId()
        //{
        //    Random rnd = new Random();
        //    int id = 1;
        //    if (BonusCards.Count!=999999)
        //    {
        //        while (BonusCards.Find(x => x.Id == id) != null)
        //        {
        //            id = rnd.Next(1, 100000);
        //        }
        //    }
        //    else
        //    {
        //        throw new Exception("id's limit is over");
        //    }
        //    return id;
        //}

        private int GetNewId(int from, int to) 
        {
            if (BonusCards.Count == MaxLimitCard + 1) 
            {
                throw new Exception("id's limit is over");
            }
            BonusCards = BonusCards.OrderBy(x => x.Id).ToList();
            return GenerateId(from, to);
        }

        private int GenerateId(int from, int to) 
        {
            int id = 0;
            Random rnd = new Random();
            if (BonusCards.Count == 2)
            {
                return rnd.Next(from + 1, to-1);
            }
            else
            {
                (BonusCard nextMinId, BonusCard curId, BonusCard nextMaxId) res = GetRandomIndex();

                while (id == 0)
                {
                    if (res.nextMaxId.Id - res.curId.Id > 1)
                    {
                        id = rnd.Next(res.curId.Id + 1, res.nextMaxId.Id);
                    }
                    else
                    {
                        if (res.curId.Id - res.nextMinId.Id > 1)
                        {
                            id =  rnd.Next(res.nextMinId.Id+1, res.curId.Id);
                        }
                        else
                        {
                            res = GetRandomIndex();
                        }
                    }
                }
                return id;
            }            
        }

        private (BonusCard nextMinId, BonusCard curId, BonusCard nextMaxId) GetRandomIndex() 
        {
            Random rnd = new Random();
            int num = rnd.Next(1, BonusCards.Count()-1);
            var currentNum = BonusCards[num];
            var nextMaxNum = BonusCards[num + 1];
            var nextMinNum = BonusCards[num - 1];
            return (nextMinNum, currentNum, nextMaxNum);
        }

        public BonusCard FindBonusCardByPhone(string number)
        {
            return BonusCards.Find(x => x.Customer.Phone == number);        
        }

        public BonusCard FindBonusCard(int id)
        {
            return BonusCards.Find(x => x.Id == id);
        }

        public void Save() 
        {
            XmlSerializer writer = new XmlSerializer(typeof(List<BonusCard>));
            var path = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + $"//{File}.xml";
            System.IO.FileStream file = System.IO.File.Create(path);

            writer.Serialize(file, BonusCards);
            file.Close();
        }

        public Manager FillSampleBonusCardBaseFile()
        {
            var path = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + $"//{"BonusCards_test"}.xml";
            System.IO.File.Delete(path);

            if (this.BonusCards.Count != 1000000)
            {
                for (int i = 1; i < 999999; i++)
                {
                    this.AddNewBonusCard($"Client{i.ToString()}", i.ToString(), Convert.ToDouble(i), i);
                }
                this.Save();
            }
            return this;
        }
    }
}
