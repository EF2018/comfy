﻿using System;

namespace Comfy.RazorClassLibrary.Models
{
    public class Operation
    {
        public int IdCard { get; set; }
        public OperationType Type { get; set; }
        public double Sum { get; set; }
        public DateTime Date { get; set; }
        public double BalanceAfterTransaction { get; set; }
    }
}
