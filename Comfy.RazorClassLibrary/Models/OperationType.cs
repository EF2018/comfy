﻿namespace Comfy.RazorClassLibrary.Models
{
    public enum OperationType
    {
        Credit = 1,
        Debit = 2
    }
}
