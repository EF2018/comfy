﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace Comfy.RazorClassLibrary.Models
{

    [Serializable]
    public class BonusCard
    {
        public Customer Customer { get; set; }
        public int Id { get; set; } 
        public DateTime ExpiryDate { get; set; }
        public double Balance { get; set; }
        public List<Operation> Operations { get; set; } = new List<Operation>();

        public BonusCard()
        {

        }

        public BonusCard(Customer customer, DateTime dateExpire, double balance)
        {
            Customer = customer;
            ExpiryDate = dateExpire;
            Balance = balance;
        }

        public void MakeTransaction(double sum, OperationType type) 
        {
            if (DateTime.Now < ExpiryDate)
            {
                if (type == OperationType.Debit)
                {
                    Balance -= sum;
                    this.Operations.Add(new Operation() { Date = DateTime.Now, Sum = -sum, Type = OperationType.Debit, BalanceAfterTransaction = Balance });
                }
                if (type == OperationType.Credit)
                {
                    Balance += sum;
                    this.Operations.Add(new Operation() { Date = DateTime.Now, Sum = sum, Type = OperationType.Credit, BalanceAfterTransaction = Balance });
                }
            }             
        }

        public void Save() 
        {
            XmlSerializer writer = new XmlSerializer(typeof(BonusCard));
            var path = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + $"//BonusCards.xml";
            System.IO.FileStream file = System.IO.File.Create(path);

            writer.Serialize(file, this);
            file.Close();
        }
    }
}
