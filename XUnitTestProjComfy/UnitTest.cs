using Comfy.RazorClassLibrary.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;
using Xunit;

namespace XUnitTestProjComfy
{
    public class UnitTest
    {
        public Manager Mng { get; set; }
        public UnitTest()
        {
            Mng = Manager.GetInstance("BonusCards_test", 999999);
            Mng.FillSampleBonusCardBaseFile();
        }

        [Fact]
        public void CreateBonusCardTest()
        {
            Assert.Equal(25637, Mng.BonusCards[25638].Id);
            Mng.DeleteBonusCard(25637);
            Assert.NotEqual(25637, Mng.BonusCards[25638].Id);
            Mng.AddNewBonusCard("test customer", "test phone", 340,00);
            Assert.Equal("test customer", Mng.BonusCards[25637].Customer.Name);
            Mng.BonusCards.Clear();

            Assert.Empty(Mng.BonusCards);

            var serializer = new XmlSerializer(typeof(List<BonusCard>));
            var path = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + $"//{"BonusCards_test"}.xml";
            using (StreamReader sr = new StreamReader(path))
            {
                Mng.BonusCards.AddRange((List<BonusCard>)serializer.Deserialize(sr));
            }
            Assert.Equal("test customer", Mng.BonusCards[25637].Customer.Name);
        }

        [Fact]
        public void OverflowBonusCardListTest()
        {
            var ex = Assert.Throws<Exception>(() => Mng.AddNewBonusCard("test", "test", 0));//CreateRandomListBonusCards(mng));
            Assert.Equal("id's limit is over", ex.Message);
        }

        [Fact]
        public void FindAndCreateLastBonusCard() 
        {
            Mng.DeleteBonusCard(258262);
            Assert.Equal(999999, Mng.BonusCards.Count);
            int id = Mng.AddNewBonusCard("last", "last", 0);
            Assert.Equal(258262, Mng.FindBonusCard(258262).Id);            
        }


        private void CreateRandomListBonusCards(Manager mng) 
        {
            for (int i = 0; i <= mng.MaxLimitCard; i++)
            {
                mng.AddNewBonusCard("name", "phone", i);
            }
        }

        private bool VerifyOrder(List<BonusCard> list) 
        {

            for (int i = 0; i < list.Count; i++)
            {
                if (list[i].Id != i)
                {
                    return false;
                }
            }
            return true;
        }

        [Fact]
        public void FindBonusCardByIdTest()
        {
            BonusCard card = Mng.FindBonusCard(34556);
            Assert.Equal(34556, card.Id);
        }
    }
}
