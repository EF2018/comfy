﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Comfy.Models
{
    public class BonusCardViewModel
    {
        //some changes3
        [Display(Name = "Idss card")]//changes from master
        [DisplayFormat(DataFormatString = "{0:000000}")]
        public int Id { get; set; }

        //some changes4
        [Display(Name = "Client name")]//some changes from dev
        //some changes2 from dev
        [Required(ErrorMessage = "Please, enter client name")]
        public string Name { get; set; }

        [Display(Name = "Phone number")]
        [Required(ErrorMessage = "Please, enter client phone number")]
        [MaxLength(10, ErrorMessage = "Phone number must be consist 10 numbers")]
        [MinLength(10, ErrorMessage = "Phone number must be consist 10 numbers")]
        [DataType(DataType.PhoneNumber)]
        public string PhoneNumber { get; set; }

        [Display(Name = "Card balance")]
        [Required(ErrorMessage = "Please, enter start bonus card balance")]
        [Range(0.00, 9999.99)]
        [DisplayFormat(DataFormatString = "{0:F2}")]
        public double Balance { get; set; }

        [Display(Name = "Card expire date")]
        [Required(ErrorMessage = "Please, enter bonus card expiry date")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd-MM-yyyy hh:mm:ss}", ApplyFormatInEditMode = true)]
        public DateTime ExpireDate { get; set; }
    }
}
