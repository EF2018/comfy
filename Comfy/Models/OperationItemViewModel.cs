﻿using Comfy.RazorClassLibrary.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Comfy.Models
{
    public class OperationItemViewModel
    {
        public int Id { get; set; }
        [Display(Name = "Date and time operation")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd-MM-yyyy hh:mm:ss}")]
        public string Date { get; set; }
        [DisplayFormat(DataFormatString = "{0:F2}")]
        public double Balance { get; set; }
        [Display(Name = "Type")]
        public string Type { get; set; }
        [Display(Name = "Sum operation")]
        public double Sum { get; set; }
        [Display(Name = "Balance after operation")]
        [DisplayFormat(DataFormatString = "{0:F2}")]
        public double AfterOperationBalance { get; set; }
    }
}
