﻿using Comfy.RazorClassLibrary.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Comfy.Models
{
    public class OperationViewModel
    {
        [Display(Name = "Id bonus card")]
        public int IdBonusCard { get; set; }

        [Display(Name = "Client name")]
        public string Name { get; set; }

        [Display(Name = "Client phone")]
        public string PhoneNumber { get; set; }

        [Display(Name = "Current card balance")]
        public double CurrentBalance { get; set; }

        [Display(Name = "Expire date card")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd-MM-yyyy hh:mm:ss}")]
        public string ExpireDate { get; set; }

        [Display(Name = "Operation type")]
//        [RequiredEnumFieldAttribute( ErrorMessage = "Please, choose operation type")]
//        [Required(ErrorMessage = "Please, type transaction sum")]
        public OperationType Type { get; set; }

        [Display(Name = "Operation sum")]
        [Required(ErrorMessage = "Please, type transaction sum")]
        [Range(0.01, 9999.99, ErrorMessage ="Sum must be in the range from 0 up to 9999.99")]
        [DisplayFormat(DataFormatString = "{0:F2}")]
        public double Sum { get; set; }

        [Display(Name = "Card operations")]
        public List<OperationItemViewModel> Operations { get; set; } = new List<OperationItemViewModel>();
    }
}
