﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Comfy.Models;
using Comfy.RazorClassLibrary.Models;

namespace Comfy.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private Manager Manager => Manager.GetInstance("BonusCards_test", 999999);
        
        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        public ActionResult Index(ManageMessageId? message, string id = "-1", string phone = "")
        {
            ViewBag.StatusMessage =
                message == ManageMessageId.AddedBonusCard ? "New card added."
                : message == ManageMessageId.DeleteBonusCard ? "Card was deleted."
                : message == ManageMessageId.AddedTransactionBonusCard ? "Transaction was added."
                : message == ManageMessageId.BaseCompleted ? "Cards base has already completed."
                : message == ManageMessageId.BaseFileWasClear ? "Cards base was cleared."
                : message == ManageMessageId.BaseFileWasFilled ? "Cards base filled."
                : message == ManageMessageId.EdittedBonusCard ? "Card was editted."
                : message == ManageMessageId.Error ? "Incorrect search input"
                : "";

            List<BonusCardViewModel> list = new List<BonusCardViewModel>();
            ViewData["TotalCards"] = Manager.BonusCards.Count;

            if (id != null && phone!=null|| id != null && phone == null|| id == null && phone != null) 
            {
                try
                {
                    int idnew = id == null ? -1 : Convert.ToInt32(id);
                    var cards = Manager.BonusCards.Where(x => x.Id == idnew || x.Customer.Phone == phone).ToList();
                    foreach (var card in cards)
                    {
                        BonusCardViewModel cardVM = new BonusCardViewModel()
                        {
                            Id = card.Id,
                            Balance = card.Balance,
                            ExpireDate = card.ExpiryDate,
                            Name = card.Customer.Name,
                            PhoneNumber = card.Customer.Phone
                        };
                        list.Add(cardVM);
                    }

                }
                catch (Exception)
                {
                    return RedirectToAction("Index", new { message = ManageMessageId.Error });
                }
            }
            return View(list);
        }

        public ActionResult CreateBonusCard()
        {
            if (Manager.BonusCards.Count<Manager.MaxLimitCard)
            {
                ViewBag.Title = $"Adding new card";
                return View();
            }
            return RedirectToAction("Index", new { message = ManageMessageId.BaseCompleted });
        }

        [HttpPost]
        public ActionResult CreateBonusCard(BonusCardViewModel card)
        {
            int id = Manager.AddNewBonusCard(card.Name, card.PhoneNumber, card.Balance);            
            return RedirectToAction("Index", new { message = ManageMessageId.AddedBonusCard, id = id });
        }

        public ActionResult EditBonusCard(int id)
        {
            BonusCard card = Manager.FindBonusCard(id);
            BonusCardViewModel model = new BonusCardViewModel() { Id = card.Id, Balance = card.Balance, ExpireDate = card.ExpiryDate, Name = card.Customer.Name, PhoneNumber = card.Customer.Phone }; 
            ViewBag.Title = $"Editing card";
            return View("EditBonusCard", model);
        }

        [HttpPost]
        public ActionResult EditBonusCard([Bind("Id,Balance,Name,PhoneNumber,ExpireDate")] BonusCardViewModel newcard)
        {
            if (ModelState.IsValid)
            {
                BonusCard oldcard = Manager.FindBonusCard(newcard.Id);
                oldcard.Balance = newcard.Balance;
                oldcard.Customer.Name = newcard.Name;
                oldcard.Customer.Phone = newcard.PhoneNumber;
                oldcard.ExpiryDate = newcard.ExpireDate;
                Manager.Save();
                return RedirectToAction("Index", new { message = ManageMessageId.EdittedBonusCard, id = newcard.Id });
            }
            return View("EditBonusCard", newcard);
        }

        public ActionResult AddTransactionBonusCard(int id)
        {
            BonusCard card = Manager.FindBonusCard(id);
            if (card.ExpiryDate>DateTime.Now)
            {
                List<OperationItemViewModel> operations = new List<OperationItemViewModel>();
                if (card.Operations != null)
                {
                    operations = card.Operations.OrderBy(x => x.Date)
                    .Select(x => new OperationItemViewModel()
                    {
                        Id = x.IdCard,
                        Date = x.Date.ToString(),
                        Sum = x.Sum,
                        Type = x.Type.ToString(),
                        AfterOperationBalance = x.BalanceAfterTransaction
                    })
                    .ToList();
                }

                OperationViewModel operation = new OperationViewModel()
                {
                    IdBonusCard = card.Id,
                    ExpireDate = card.ExpiryDate.ToString(),
                    CurrentBalance = card.Balance,
                    Name = card.Customer.Name,
                    PhoneNumber = card.Customer.Phone,
                    Operations = operations
                };
                ViewBag.Title = $"Adding card transaction";
                return View("AddTransactionBonusCard", operation);
            }
            return BadRequest("Сard expired.");
        }

        [HttpPost]
        public ActionResult AddTransactionBonusCard(OperationViewModel operation)
        {
            if (ModelState.IsValid)
            {
                BonusCard card = Manager.FindBonusCard(operation.IdBonusCard);
                card.MakeTransaction(operation.Sum, operation.Type);
                Manager.Save();
                return RedirectToAction("Index", new { message = ManageMessageId.AddedTransactionBonusCard, id = operation.IdBonusCard });
            }
            return View("AddTransactionBonusCard", operation);
        }
        
        public ActionResult FillBaseCards()
        {
            Manager.FillSampleBonusCardBaseFile();
            return RedirectToAction("Index", new { Message = ManageMessageId.BaseFileWasFilled });
        }

        public ActionResult ClearBaseCard() 
        {
            Manager.BonusCards.Clear();
            Manager.Save();
            return RedirectToAction("Index", new { message = ManageMessageId.BaseFileWasClear });
        }

        public ActionResult DeleteBonusCard(string id)
        {
            int newid = Convert.ToInt32(id);
            Manager.DeleteBonusCard(newid);
            return RedirectToAction("Index", new { Message = ManageMessageId.DeleteBonusCard });
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }

    public enum ManageMessageId
    {
        AddedBonusCard,
        EdittedBonusCard,
        DeleteBonusCard,
        BaseFileWasFilled,
        BaseFileWasClear,
        AddedTransactionBonusCard,
        BaseCompleted,
        Error
        //NoResult,
    }
}
